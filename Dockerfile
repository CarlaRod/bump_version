FROM golang:alpine

RUN mkdir -p $GOPATH/src/script 
ADD . $GOPATH/src/script
WORKDIR $GOPATH/src/script 

RUN go build -o bump . 

CMD ["/go/src/script/bump"]